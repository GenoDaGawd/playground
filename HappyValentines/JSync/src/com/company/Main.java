package com.company;



import javax.xml.transform.Source;
import java.io.*;
import java.nio.channels.FileChannel;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    ExecutorService pool = Executors.newFixedThreadPool(500);
    public static void main(String[] args) {
        File source = new File("C:\\Users\\piet\\Music");
        File destination = new File("C:\\Users\\piet\\Sync");

        Main main = new Main();

        if (source.isDirectory()){

            main.sync(source,destination);

        }else{
            System.out.println("The path specified is not a directoy");
        }
    }

    public void sync(File source,File destination){

        if (source.isDirectory()){

            File[] files = source.listFiles();

            for (int i=0;i<files.length;i++){
                String fileDestination= destination.getAbsolutePath();
                File file =files[i];
                if (file.isDirectory()){
                    fileDestination += "\\" + file.getName();

                    sync(new File(file.getAbsolutePath()),new File(fileDestination));
                }else{

                    fileDestination += "\\" + file.getName();

                    Callable<Void> task = new Copier(file,new File(fileDestination));
                    this.pool.submit(task);

                }
            }

        }else{

        }
    }

    class Copier implements Callable<Void>{
        File destFile,sourceFile;

        Copier(File sourceFile,File destFile){
            this.sourceFile=sourceFile;
            this.destFile=destFile;
        }

        @Override
        public Void call() {



            if(!destFile.exists()) {

                try {

                    destFile.getParentFile().mkdirs();
                    destFile.createNewFile();

                    FileChannel source = null;
                    FileChannel destination = null;

                    try {

                        source = new FileInputStream(sourceFile).getChannel();
                        destination = new FileOutputStream(destFile).getChannel();
                        destination.transferFrom(source, 0, source.size());
                        System.out.println("Copying: "+ sourceFile.getAbsolutePath()+" to: "+destFile.getAbsolutePath());

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }  finally {
                        if(source != null) {
                            try {
                                source.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if(destination != null) {
                            try {
                                destination.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                System.out.println("This file was skipped it already exists : " +sourceFile.getAbsolutePath());
            }

            return null;
        }
    }

}
