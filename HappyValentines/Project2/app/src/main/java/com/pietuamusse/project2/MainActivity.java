/**
 * Add your package below. Package name can be found in the project's AndroidManifest.xml file.
 * This is the package name our example uses:
 *
 * package com.example.android.justjava; 
 */
package com.pietuamusse.project2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;


/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    TextView priceTextView;
    TextView quantityTextView;
    int quantity = 0;
    int price = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        priceTextView = (TextView) findViewById(R.id.price_text_view);
        quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantity = Integer.parseInt(quantityTextView.getText().toString());
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {

    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {

        quantityTextView.setText("" + number);
    }

    /**
     * This method displays the given price on the screen.
     */
    private void displayPrice(int number) {

        priceTextView.setText("Total : $"+number);
    }

    public void incrementQuantity(View view){
        quantity = 1 +  Integer.parseInt(quantityTextView.getText().toString());
        display(quantity);
        displayPrice(price*quantity);
    }

    public void decrementQuantity(View view){
        quantity = Integer.parseInt(quantityTextView.getText().toString()) - 1;

        if (!(quantity<0)) {
            display(quantity);
            displayPrice(price * quantity);
        }
    }
}