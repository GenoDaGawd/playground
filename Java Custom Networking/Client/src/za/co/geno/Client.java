package za.co.geno;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by piet on 1/19/2017.
 */
public class Client {
    public static void main(String[] args) {
        SSLSocketFactory sslSocketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        try {
            Socket sslSocket = sslSocketFactory.createSocket("10.10.10.68", 9384);

            OutputStream out = sslSocket.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);

            outputStreamWriter.write("testing");

            outputStreamWriter.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
