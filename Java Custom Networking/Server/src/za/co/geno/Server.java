package za.co.geno;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by piet on 1/19/2017.
 */
public class Server {

    public static void main(String[] args) {

        try (ServerSocket server = new ServerSocket(7001)) {
            while (true) {
                try  {
                    Socket socket = server.accept();

                    Thread request = new RequestHandler(socket);

                    request.start();

                } catch (IOException e) {

                    e.printStackTrace();

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static class RequestHandler extends Thread{

        Socket socket;

        RequestHandler(Socket socket){
            this.socket=socket;
        }

        @Override
        public void run() {
            try {

                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                System.out.println(reader.readLine());

            }catch(IOException e){
                e.printStackTrace();
            }finally {
                if (socket!=null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

}
